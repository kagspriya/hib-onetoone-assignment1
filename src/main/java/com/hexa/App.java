package com.hexa;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class App 
{
    public static void main( String[] args )
    {
    	StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
        Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  
          
        SessionFactory factory=meta.getSessionFactoryBuilder().build();  
        Session session=factory.openSession();  
          
        Transaction t=session.beginTransaction();   
        
        Colours colour1 = new Colours();
        colour1.setColour_name("red");
        colour1.setColour_intensity(10);
        
        Fruits fruit1 = new Fruits();
        fruit1.setFruit_name("apple");
        fruit1.setFruit_ph(4);
        fruit1.setFruit_flavour("sweet");
        fruit1.setFruit_nutrient("Vitamin C,K");
        
        colour1.setFt(fruit1);
        fruit1.setClr(colour1);
        
        session.persist(colour1);
        
        t.commit();
        session.close();
        System.out.println("The colour and fruit datas are inserted into the table using one-to-one relationship");
        
          
    	
    	
    }
}
