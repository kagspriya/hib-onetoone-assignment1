package com.hexa;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="ColoursOTO")
public class Colours {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="colour_id")
	private int colour_id;
	@Column(name="colour_name")
	private String colour_name;
	@Column(name="colour_intensity")
	private float colour_intensity;
	
	@OneToOne(targetEntity=Fruits.class,cascade=CascadeType.ALL)  
	private Fruits ft;
	
	public Colours() { }
	public Colours(int colour_id, String colour_name, float colour_intensity, Fruits ft) {
		super();
		this.colour_id = colour_id;
		this.colour_name = colour_name;
		this.colour_intensity = colour_intensity;
		this.ft = ft;
	}
	public Fruits getFt() {
		return ft;
	}
	public void setFt(Fruits ft) {
		this.ft = ft;
	}
	
	@Override
	public String toString() {
		return "Colours [colour_id=" + colour_id + ", colour_name=" + colour_name + ", colour_intensity="
				+ colour_intensity + ", ft=" + ft + "]";
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ft == null) ? 0 : ft.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colours other = (Colours) obj;
		if (ft == null) {
			if (other.ft != null)
				return false;
		} else if (!ft.equals(other.ft))
			return false;
		return true;
	}
		
	
	public int getColour_id() {
		return colour_id;
	}
	public void setColour_id(int colour_id) {
		this.colour_id = colour_id;
	}
	public String getColour_name() {
		return colour_name;
	}
	public void setColour_name(String colour_name) {
		this.colour_name = colour_name;
	}
	public float getColour_intensity() {
		return colour_intensity;
	}
	public void setColour_intensity(float colour_intensity) {
		this.colour_intensity = colour_intensity;
	}
	
	
	
}
