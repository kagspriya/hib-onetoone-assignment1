package com.hexa;
import java.util.Iterator;    
import java.util.List;  
  
import javax.persistence.TypedQuery;    
import org.hibernate.Session;    
import org.hibernate.SessionFactory;  
import org.hibernate.boot.Metadata;  
import org.hibernate.boot.MetadataSources;  
import org.hibernate.boot.registry.StandardServiceRegistry;  
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;   

public class Fetch {

	public static void main(String[] args) {
		 StandardServiceRegistry ssr=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		    Metadata meta=new MetadataSources(ssr).getMetadataBuilder().build();  
		      
		    SessionFactory factory=meta.getSessionFactoryBuilder().build();  
		    Session session=factory.openSession();  
		        
		    TypedQuery query=session.createQuery("from Colours c");    
		    List<Colours> list=query.getResultList();   
		        
		    Iterator<Colours> itr=list.iterator();    
		    while(itr.hasNext()){    
		    	Colours clr=itr.next();    
		     System.out.println(clr.getColour_id()+" "+clr.getColour_intensity()+" "+clr.getColour_name());  
		     
		     Fruits ft = clr.getFt();    
		     System.out.println(ft.getFruit_no()+" "+ft.getFruit_name()+" "+ ft.getFruit_flavour()+" "+ft.getFruit_nutrient()+" "+ft.getFruit_ph());    
		    }    
		    
		    session.close();    
		    System.out.println("The datas in the table colours and fruits are successfully fetched");

	}

}
