package com.hexa;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="FruitsOTO")
public class Fruits {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	@Column(name="fruit_no")
	private int fruit_no;
	@Column(name="fruit_name")
	private String fruit_name;
	@Column(name="fruit_ph")
	private float fruit_ph;
	@Column(name="fruit_flavour")
	private String fruit_flavour;
	@Column(name="fruit_nutrient")
	private String fruit_nutrient;
	
	@OneToOne(targetEntity=Colours.class)
	private Colours clr;
	
	
	public Fruits() { }
	
	public Fruits(int fruit_no, String fruit_name, float fruit_ph, String fruit_flavour, String fruit_nutrient,
			Colours clr) {
		super();
		this.fruit_no = fruit_no;
		this.fruit_name = fruit_name;
		this.fruit_ph = fruit_ph;
		this.fruit_flavour = fruit_flavour;
		this.fruit_nutrient = fruit_nutrient;
		this.clr = clr;
	}


	
	
	@Override
	public String toString() {
		return "Fruits [fruit_no=" + fruit_no + ", fruit_name=" + fruit_name + ", fruit_ph=" + fruit_ph
				+ ", fruit_flavour=" + fruit_flavour + ", fruit_nutrient=" + fruit_nutrient + ", clr=" + clr + "]";
	}

	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clr == null) ? 0 : clr.hashCode());
		result = prime * result + ((fruit_flavour == null) ? 0 : fruit_flavour.hashCode());
		result = prime * result + ((fruit_name == null) ? 0 : fruit_name.hashCode());
		result = prime * result + fruit_no;
		result = prime * result + ((fruit_nutrient == null) ? 0 : fruit_nutrient.hashCode());
		result = prime * result + Float.floatToIntBits(fruit_ph);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruits other = (Fruits) obj;
		if (clr == null) {
			if (other.clr != null)
				return false;
		} else if (!clr.equals(other.clr))
			return false;
		if (fruit_flavour == null) {
			if (other.fruit_flavour != null)
				return false;
		} else if (!fruit_flavour.equals(other.fruit_flavour))
			return false;
		if (fruit_name == null) {
			if (other.fruit_name != null)
				return false;
		} else if (!fruit_name.equals(other.fruit_name))
			return false;
		if (fruit_no != other.fruit_no)
			return false;
		if (fruit_nutrient == null) {
			if (other.fruit_nutrient != null)
				return false;
		} else if (!fruit_nutrient.equals(other.fruit_nutrient))
			return false;
		if (Float.floatToIntBits(fruit_ph) != Float.floatToIntBits(other.fruit_ph))
			return false;
		return true;
	}

	
	
	public int getFruit_no() {
		return fruit_no;
	}

	public void setFruit_no(int fruit_no) {
		this.fruit_no = fruit_no;
	}

	public String getFruit_name() {
		return fruit_name;
	}

	public void setFruit_name(String fruit_name) {
		this.fruit_name = fruit_name;
	}

	public float getFruit_ph() {
		return fruit_ph;
	}

	public void setFruit_ph(float fruit_ph) {
		this.fruit_ph = fruit_ph;
	}

	public String getFruit_flavour() {
		return fruit_flavour;
	}

	public void setFruit_flavour(String fruit_flavour) {
		this.fruit_flavour = fruit_flavour;
	}

	public String getFruit_nutrient() {
		return fruit_nutrient;
	}

	public void setFruit_nutrient(String fruit_nutrient) {
		this.fruit_nutrient = fruit_nutrient;
	}

	public Colours getClr() {
		return clr;
	}

	public void setClr(Colours clr) {
		this.clr = clr;
	}

	
	
	
}